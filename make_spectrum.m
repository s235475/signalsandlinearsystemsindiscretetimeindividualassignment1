function [Y, freq] = make_spectrum(signal, fs)
% Function call:
%
% >> [Y, freq] = make_spectrum(signal, fs)
%
% Computes the fft of the signal and returns it in the variable Y
% Returns the corresponding frequency vector
Y = fft(signal);
Y = Y/(length(Y));
n = length(signal);
T = n/fs;
delta_f = 1/T;
if (rem(n,2) == 0)
    freq_pos = 0:delta_f:fs/2;
    freq_neg = -fs/2+delta_f:delta_f:-delta_f;
else
    freq_pos = 0:delta_f:fs/2 - delta_f/2;
    freq_neg = -fs/2+delta_f/2:delta_f:-delta_f;
end
freq = [freq_pos, freq_neg];
Y = Y(:);
freq = freq(:);
end